package INF101.lab2.pokemon;

public class Main {

    public static Pokemon pokemon1;
    public static Pokemon pokemon2;
    public static void main(String[] args) {
        ///// Oppgave 6
        // Have two pokemon fight until one is defeated
        pokemon1 = new Pokemon("Pikachu", 94, 12);
        pokemon2 = new Pokemon("Oddish", 100, 3);

        while (pokemon1.isAlive() && pokemon2.isAlive() == true) {
            pokemon1.attack(pokemon2);
            if (pokemon2.isAlive()) pokemon2.attack(pokemon1);
        }
    }
}
